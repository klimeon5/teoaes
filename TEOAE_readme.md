# TEOAEs

The main function for measuring TEOAE is the teoaeMain.m

1. Start the teoaeMain.m and the GUI will open
2. Adjust the parameters for zou purposes.
3. Change the Click Calibration to CGLS variant.
4. Start the measurement with TEOAE button.

The click is generated in genCEOAEclick.m function. 

5. The results can be plot with teoae_cgls_PlotResultsMerge.m script.

The click that we send to the probe is the Stimulus variable in Calibration/CGLS_reconst_click_long_tube. The Stimulus was calculated in ClickCalib.m with CGLS method based on DiracResp variable and CEOAEBlock (Gaussian pulse).