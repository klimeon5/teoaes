function [RecOAE, unrun] = presentTEOAEs(CEOAE4000Blocks,fsamp)
% function [f1tone, f2tone] = presentDPOAEs(F2values,F1values,L2values,L1values,fs,Tdur,Rdur,gen);
% PRESENTS DPOAE STIMULI TO A SOUNDCARD AND GIVES THE RECORDED SIGNAL AS AN
% OUTPUT
%




%% send it to the SOUND CARD and record

%% Bures
% 
% gen.recordSound(0);
% 
% gen.playSound(([f1tone  f2tone]'), [0 1], 0, 0); % send sound to a sound card
% 
% pause(dur);
% gen.stopSound();
% gen.stopRecording();
% 
% recordedSignal = gen.getRecData(1); % save recorded data

%% DSP audio

% F2values = 1e3;
% F1values = 1e3;
% L1values= 50;
% L2values = 50;
% fs = 102.4e3;
% Tdur = 4;
% Rdur = 0.05;
% 
% [f1tone, f2tone] = createDPOAEstimuli(F2values,F1values,L2values,L1values,fs,Tdur,Rdur);
% 
% addpath('DSPaudioplayer/');
% 
% H = dsp.AudioPlayer
% 
% devID = 'default';                    %device ID The default is Default, which 
%                                       %is the computer's standard output device. 
%                                       %You can use tab completion to query valid 
%                                       %DeviceName assignments for your computer by
%                                       %typing H.DeviceName = ' and then pressing the tab key.
% bitDepth = '24-bit integer';  %output bitdepth
% outputCh = [1,2];             %vector containing output channels' numbers
% bufferSize = 2048;
% 
% 
% 
% 
% data.fs = 48e3;
% Fs= data.fs;
% playDeviceID = 'default';
% recDeviceID = 'default';
% RecChanList = 1; %str2double(get(handles.recChan,'string'));
% PlayChanList = [1,2];
% 
% recData = DSPplayANDrec([f1tone,f2tone],playDeviceID,PlayChanList,recDeviceID,RecChanList,bufferSize,Fs);
% 

%% DSP newer


% F2values = 1e3;
% F1values = 1e3;
% L1values= 50;
% L2values = 50;
% fs = 102.4e3;
% Tdur = 4;
% Rdur = 0.05;
% 
% [f1tone, f2tone] = createDPOAEstimuli(F2values,F1values,L2values,L1values,fs,Tdur,Rdur);
% 
% fileReader = dsp.AudioFileReader('pokus.wav');
%        devWriter = audioDeviceWriter('Driver','ASIO');
%       fileInfo = audioinfo(fileReader.Filename);
%        setup(devWriter, zeros(fileReader.SamplesPerFrame, ...
%                        fileInfo.NumChannels));  % Initialize audio device
%        while ~isDone(fileReader)
%            audioIn = step(fileReader);
%            nUnderrun = play(devWriter, audioIn);
%            if nUnderrun > 0
%              fprintf('Audio writer queue was underrun by %d samples.\n',...
%                        nUnderrun);
%            end
%        end
%        fprintf('Latency due to sound card''s output buffer: %f seconds.\n', ...
%                 fileReader.SamplesPerFrame/devWriter.SampleRate);
%        release(fileReader);            % close the input file
%        release(devWriter);             % release the audio device

DSP = 2;


if DSP==0

% DAQ

% 
% % create DirectSound session
% session = daq.createSession('directsound');
% 
% % get devices and set the input and output device
% devices = daq.getDevices;
% 
% % input_device = devices(3);
% input_device = devices(10); % Mic input - settings for lab n. 541
% 
% output_device = devices(13);
% %output_device = devices(12); % Phones - settings for lab n. 541
% 
% nchan = 2;
% 
% % add input and output channels
% chi=addAudioInputChannel(session,input_device.ID,1);
% % chi=addAudioInputChannel(session,input_device.ID,1);
% cho=addAudioOutputChannel(session,output_device.ID,1:nchan);
% 
% %fsamp = 96e3;
% session.Rate = fsamp;
% %session.IsContinuous = true;
% outputData = zeros(length(f1tone),nchan);
% 
% %Prepare data for output
% queueOutputData(session, [f1tone f2tone]);
% 
% %Preparing session - reduce latency
% prepare(session);
% 
% %starting Data Aquisition and recording data
% %[data,timeStamps,triggerTime] = startForeground(session);
% [RecOAE,timeStamps,triggerTime] = startForeground(session);
% 
% stop(session);
% 
% % session.IsContinuous = false;
% % release DirectSound session
% release(session);

elseif DSP==1 % DSP toolbox
    
    H = dsp.AudioPlayer

    devID = 'default';                    %device ID The default is Default, which 
                                     %is the computer's standard output device. 
                                      %You can use tab completion to query valid 
                                      %DeviceName assignments for your computer by
                                      %typing H.DeviceName = ' and then pressing the tab key.
    bitDepth = '24-bit integer';  %output bitdepth
    %outputCh = [1,2];             %vector containing output channels' numbers
    bufferSize = 2048;
    % 
    % data.fs = 48e3;
    Fs= fsamp;
    playDeviceID = 'default';
    recDeviceID = 'default';
    RecChanList = 1; %str2double(get(handles.recChan,'string'));
    PlayChanList = [3,4];
    setpref('dsp','portaudioHostApi',3)
    [RecOAEa, unrun] = DSPplayANDrec([f1tone f2tone (f1tone + f2tone)/10],playDeviceID,PlayChanList,recDeviceID,RecChanList,bufferSize,Fs);

    RecOAE = RecOAEa;%(:,1);

elseif DSP==2
    
   
    inputCh = [1,2] ; %str2double(get(handles.recChan,'string'));
    outputCh = [3,4];
    

    
    
    %pahandle = PsychPortAudio(�Open� [, deviceid][, mode][, reqlatencyclass][, freq][, channels][, buffersize][, suggestedLatency][, selectchannels][, specialFlags=0]);
    
%     f1tone = [f1tone; zeros(15e3,1)]; % silence added due to time latency
%     f2tone = [f2tone; zeros(15e3,1)];
%     InputSig = [f1tone f2tone (f1tone + f2tone)/10];
%     InputSig=CEOAE4000Blocks;
InputSig = [CEOAE4000Blocks; zeros(1,length(CEOAE4000Blocks)); CEOAE4000Blocks]';

    buffersize = 2048;
    input = PPAplayANDrec(InputSig,outputCh,inputCh,buffersize, fsamp);
    
    
    RecOAE = input(:,[1 3]);

    
%     if Status.XRuns>0
%         fprintf('Status: xrun detected\n');
%     end
%     
%     if xruns>0
%         fprintf('xrun detected\n');
%     end
    
end