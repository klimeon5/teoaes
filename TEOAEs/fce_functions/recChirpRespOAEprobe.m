function [Pecs, Amp, Hres] = recChirpRespOAEprobe(fsamp,NumChirps,f1,f2,Nsamp,NsampCh,Amp,GainMicA,PrChann,shaped)
% function [Pecs, MCh] = recChirpRespOAEprobe(fsamp,NumChirps,f1,f2,Nsamp,Nzeros,Amp)
% record respose to a chirp signal presented to one chanel of OAE probe
%NumChirps = 256; % number of chirps
%f1 = 80; % start freq (Hz)
%f2 = 15000; % final frequency  (Hz)
%Nsamp = 4800; % number of samples of one chirp (50 ms)
%Nzeros = added zeros after each chirp
% amplitude of chirp

% please keep values of parameters to achieve integer even number of
% (f1+f2)*Nsamp/fsamp, this assures that the last point of generated chirp
% is one sample before 0 amplitude.


%Nzeros = 8192-Nsamp; % add number of zeros behind chirp
% for testing purposes, is there any difference between simulations with
% silence after chirp (Keefe used this approach) or not
% zeros after chirp could be necessary for calibration in 15m long tube
% because the short chirp response is not affected by reflection, but
% longer chirp would be. Maybe put a silence gap and than repeat
% measurement?

SAVE =0;
% generate array of chirps
yChArr = [];
Nzeros = NsampCh-Nsamp;

% if nargin<9 
%     shaped = 1; 
%     load Calibration\recTubeLLref.mat Envelope EnvelopeF
% end
shaped = 0;
for k=1:NumChirps
   
   yChirp = genLinSweptSine(f1,f2,Nsamp,fsamp);
   %yChirp = genLinSweptSineShaped(f1,f2,Nsamp,fsamp,Nzeros);
   
    if shaped % shape envelope according to response in LL tube
     yChirp = Envelope.*yChirp;
    end
   yChirp = [yChirp; zeros(Nzeros,1)];
   yChArr = [yChArr; yChirp];
    
end

%figure;
%plot(yChArr);

% shape onset and offset of chirp, in order to avoid click, however since
% it is a set of chirps, there will be clicks after each repetition

rampdur = 15e-3; % ramp duration in seconds

rampts = round(rampdur * fsamp);

step = pi/(rampts-1);
x=[0:step:pi];
offramp = (1+cos(x))./2;
onramp = (1+cos(fliplr(x)))./2;
o=ones(1,length(yChArr)-2*length(x));
wholeramp = [onramp o offramp]; % Envelope for stimulus (i.e. on/off ramps)   

yChArr = yChArr.*wholeramp.';

% adjust amplitude

%Amp = 0.1;
yChArr = yChArr*Amp; % adjust amplitude (you have to try set some value)
% idealy the level should be around 60 dB SPL?


% to choose which channel in the probe to use
if PrChann==1
    ch01 = 1;
    ch02 = 0;
elseif PrChann==2
    ch01 = 0;
    ch02 = 1;
end
    
InputSig = [ch01*yChArr ch02*yChArr yChArr];
    
outputCh = [3 4];
inputCh = [1 2];
% inputCh = [2 1];
buffersize = 2048;
RecChirp = PPAplayANDrec(InputSig,outputCh,inputCh,buffersize, fsamp);


% high pass filter

fcutoff =300; % cutoff freq of 2order high pass butterworth filter
[HPb,HPa] = butter(4,fcutoff/(fsamp/2),'high');
RecChirpF = filter(HPb,HPa,RecChirp);

% Saving recorded signals - ch.1 1/4' Mic ; ch.2 MicProbe
% audiowrite('CalibDataMicAndProbe_v5.wav',RecChirp,fsamp)

% Saving recorded signals - ch.1 OAEProbe ; ch.2 MicProbe
%audiowrite('CalibDataOAEProbeAndProbe_v4.wav',RecChirp,fsamp)


%%
smW = 20; %level of smoothing
Nonset = 4; % first Nonset chirps are not taken into account
Nstat = NumChirps-Nonset*2; % next X chirps avereaged 

%delay1 = finddelay(yChArr,RecChirp(:,2)); % find delay between chirp and recorded signal

% delay = finddelay(yChArr,RecChirpF(:,3)); % 
% delay is larger than period in the chirp signal
% hence we have to add the number of samples in the chirp
% for psychportaudio and RME fireface with the current settings, it is:
% delay = delay + Nsamp
delay = 3146; % estimated by DelayEstimate for sound card connected output input
if delay<0 % compensate for the delay
    yEar = RecChirpF(NsampCh+delay:end,1); 
else
    yEar = RecChirpF(delay:end,1);
end


yEars = yEar(Nonset*NsampCh+1:Nonset*NsampCh + Nstat*NsampCh); % selected portion of recorded signal for analysis
yEara = mean(reshape(yEars,NsampCh,Nstat).');

chirpIn = yChArr(NsampCh+1:NsampCh*2)';

fxI = (0:length(yEara)-1)*fsamp/length(yEara);
%using frequency characteristics of OAE mic
% load Calibration/CalibMicData.mat Hoaemicsens fx;
load Calibration/CalibMicDataSDist16mmYellowFoam13mm.mat Hoaemicsens fx;

if length(fxI)~=length(fx)
   Hoaemicsens = interp1(fx,Hoaemicsens,fxI,'spline');
end

Hres = (fft(yEara)./fft(chirpIn))./(Hoaemicsens.*GainMicA); % transfer char [Pa/Mat scale]
%YrecPa = Yrec./Hoaemicsens;

if shaped == 0
    Pecs = Hres.*Amp; % spectrum therefore 0.5
elseif shaped == 1
    Pecs = Hres.*EnvelopeF.'*Amp; % spectrum therefore 0.5
end

%yEarres = (fft(yEara)./fft(1/Amp*chirpIn));


%fx2 = (0:NsampCh-1)*fsamp/NsampCh;
%Hoaemicsens = interp1(fx,Hoaemicsens,fx2);

%Pecs = yEarres./(Hoaemicsens.*GainMicA);

%Pres = 

% yEarres = (fft(yEara).*fft(1/MCh*fliplr(chirpIn))/length(yEara)^2); 
% yEarres = (smooth(real(yEarres),smW,'loess') + sqrt(-1)*smooth(imag(yEarres),smW,'loess')).';


% Pecs = 2*Pecs;

if SAVE ==1
    
    uisave({'Pecs','Hres', 'yChArr', 'RecChirp', 'chirpIn', 'fsamp',...
        'NumChirps', 'f1', 'f2', 'Nsamp', 'Nzeros', 'Amp'},...
        '../Calibration/Signals/OAEchirp_.mat')
end


