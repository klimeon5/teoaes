function playChirptoEar(Amp,GainMic,NumChirps,fsamp,Nsamp,FPL,O_EPL,EarSim,Folder,SubjName,YearS,MonthS,DayS,HourS,MinuteS,SecondS,LLT)
% function for calibration in the ear (get response and do chosen
% calibration)

hwb = waitbar(0,'Sending chirps');

%NumChirps = 50;

% f1 = 80; % start freq (Hz)
% f2 = 16000; % final frequency  (Hz)
% Nsamp = 4800; % number of samples of one chirp (50 ms)
% NsampCh = 8192;

% fsamp = 44100;
f1 = 0; % start freq (Hz)
f2 = fsamp/2; % final frequency  (Hz)
% Nsamp = 2048; % number of samples of one chirp (50 ms)
NsampCh = Nsamp;
% fsamp = str2num(get(findobj('tag','editfs'),'string')).*1e3;

% Amp = 0.05;



GainMicA = 10^(GainMic/20);
% Pecs = Pec./GainMicA; % divide recorded spectrum (chirp response) 
% Hres = Hres./GainMicA;
% (fsamp,NumChirps,f1,f2,Nsamp,NsampCh,Amp,GainMicA,PrChann,shaped)
addpath('fce_functions/'); addpath('fce_stimuli/'); addpath('fce_psychportaudio/'); addpath('PsychPortAudio3/');
[Pecs1, AmpCh, Hres1] = recChirpRespOAEprobe(fsamp,NumChirps,f1,f2,Nsamp,NsampCh,Amp,GainMicA,1); %
[Pecs2, AmpCh, Hres2] = recChirpRespOAEprobe(fsamp,NumChirps,f1,f2,Nsamp,NsampCh,Amp,GainMicA,2); %
rmpath('fce_functions/'); rmpath('fce_stimuli/'); rmpath('fce_psychportaudio/'); rmpath('PsychPortAudio3/')
% fft of a chirp response in the ear.
% AmpCh - amplitude of the chirp spectrum
waitbar(1,hwb);
close(hwb);
%save Calibration/recInEarCalib.mat Pec Amp yChirp

load Calibration/CalibMicData.mat Hoaemicsens fx; % load Hoaemicsens (microphone sensitivity
% for convertion to dB SPL)

% by microphone sensitivy multiplied with the microphone gain

% check the way of calibration: SPL, FPL ...


fxI = (0:length(Pecs1)-1)*fsamp/length(Pecs1); % frequency axis

Fmin = 100; % frequency range for estimation
Fmax = 20e3; % frequency range for estimation

% Fmin = 1; % frequency range for estimation
% Fmax = 18e3; % frequency range for estimation

FminId = find(fx>=Fmin,1,'first');
FmaxId = find(fx>=Fmax,1,'first');

Pecs1 = Pecs1(FminId:FmaxId); % select only the frequency range...
Hres1 = Hres1(FminId:FmaxId);
Pecs2 = Pecs2(FminId:FmaxId); % select only the frequency range...
Hres2 = Hres2(FminId:FmaxId);

% Calibration In Ear

%Pecs Calculated
%PecsC = Hres.*AmpCh;
%    
% load Calibration/TheveninPar.mat Z0 P0 fx;
% 
% Zec = Z0.'.*Pecs./(P0.' - Pecs); % impedance of ear canal
% %ZecC = Z0.'.*PecsC./(P0.' + PecsC); % impedance of ear canal
% smW = 30;
% %Zec = (smooth(real(Zec),smW,'loess') + sqrt(-1)*smooth(imag(Zec),smW,'loess')).';
% 
% rho =  1.1769e-3; % g/cm3 air density
% eta = 1.846e-4; % g/scm shear viscosity coefficient
% a = 0.8/2; % cm radius of tubes
% c = 3.4723e4; % cm/s speed of sound 
% k = 1; 
% kC = 1;
% 
% % estimation of Zsurge
% for i=1:10
% 
%     Zsurge = k*rho*c/(pi*a^2); %surge impedance
%     ZsurgeVen(i) = Zsurge;
%     R = (Zec - Zsurge)./(Zec + Zsurge); % reflection coefficient
%     Zsouza = (Zec - Zsurge); % reflection coefficient
% %    RC = (ZecC - Zsurge)./(ZecC + Zsurge); % reflection coefficient
%     R = (smooth(real(R),smW,'loess') + sqrt(-1)*smooth(imag(R),smW,'loess')).';
% %    RC = (smooth(real(RC),smW,'loess') + sqrt(-1)*smooth(imag(RC),smW,'loess')).';
%     UpIdx = find(fxI>=16e3,1,'first');
%     if ~mod(UpIdx,2)
%         UpIdx = UpIdx+1;
%     end
% 
%     BMWwidth = UpIdx + UpIdx-1;
%     BMwin = blackman(BMWwidth)';
% 
%     Rzeros = zeros(1,4*length(fxI)/2); % upsampling 4times for inverse of R
%     Rzeros(1:ceil(BMWwidth/2))= fliplr(BMwin(1:ceil(BMWwidth/2)));
%     Rappz = [zeros(1,FminId-1) R zeros(1,length(Rzeros)-length(R)-FminId+1)];
% %     fxIex = 1:(length(R)+19);
% %     fxPex = 20:(length(R)+19);
%     fxIex = 1:(length(R)+FminId-1);
%     fxPex = FminId:(length(R)-1+FminId);
%     
%     Rir = interp1(fxPex,real(R),fxIex,'spline','extrap');
%     Rim = interp1(fxPex,imag(R),fxIex,'spline','extrap');
%     Rint = Rir + sqrt(-1)*Rim;
%     RappzI = [Rint zeros(1,length(Rzeros)-length(Rint))];
%     %   RappzC = [RC zeros(1,length(Rzeros)-length(RC))];
%     RappzI(1) = abs(RappzI(1));
%     Rall = [Rzeros.*Rappz fliplr(Rzeros(2:end)).*fliplr(conj(Rappz(2:end)))];
%     RallI = [Rzeros.*RappzI fliplr(Rzeros(2:end)).*fliplr(conj(RappzI(2:end)))];
%  %   RallC = [0 Rzeros.*RappzC fliplr(Rzeros(1:end)).*fliplr(conj(RappzC(1:end)))];
%     
%     Rtime = ifft(Rall); % give time domain R
%     RtimeI = ifft(RallI); % give time domain R
% %    RtimeC = ifft(RallC); % give time domain R
%     
% % 
% %     Zszeros = zeros(1,4*length(fxI)/2); % upsampling 4times for inverse of R
% %     Zszeros(1:ceil(BMWwidth/2))= fliplr(BMwin(1:ceil(BMWwidth/2)));
% %     Zsappz = [zeros(1,FminId-1) Zsouza.' zeros(1,length(Zszeros)-length(Zsouza)-FminId+1)];
% %  %   RappzC = [RC zeros(1,length(Rzeros)-length(RC))];
% %     fxIex = 1:(length(Zsouza)+FminId-1);
% %     fxPex = FminId:(length(Zsouza)+FminId-1);
% %     
% %     Zir = interp1(fxPex,real(Zsouza),fxIex,'spline','extrap');
% %     Zim = interp1(fxPex,imag(Zsouza),fxIex,'spline','extrap');
% %     Zint = Zir + sqrt(-1)*Zim;
% %     
% %     ZappzI = [Zint zeros(1,length(Zszeros)-length(Zint))];
% %     ZappzI(1) = abs(ZappzI(1));
% %     ZsallI = [Zszeros.*ZappzI fliplr(Zszeros(2:end)).*fliplr(conj(ZappzI(2:end)))];
% %     
% %     
% %     Zsall = [Zszeros.*Zsappz fliplr(Zszeros(2:end)).*fliplr(conj(Zsappz(2:end)))];
% %  %   RallC = [0 Rzeros.*RappzC fliplr(Rzeros(1:end)).*fliplr(conj(RappzC(1:end)))];
% %     
% %     Zstime = ifft(Zsall); % give time domain Z
% %     ZstimeI = ifft(ZsallI); % give time domain Z
% 
%     %k = k + 10*Rtime(1); %coefficient 10 empirically tested on 1 subjects
%     k = k + 10*RtimeI(1); %coefficient 10 empirically tested on 1 subjects
%  %   kC = kC + 10*RtimeC(1); %coefficient 10 empirically tested on 1 subjects
% end

fx=fxI(FminId:FmaxId); % frequency axis
        
% Pecs_withoutR = Pecs;

%Stimulus Calibration
if FPL==1 % Forward pressure level
    Pecs = Pecs./(1+R);
 %   PecsC = PecsC./(1+RC);
    
    
    % calculate Hres (transfer char)
    %load Calibration\recTubeLLref.mat k_env RecLLT
    load Calibration\recTubeLLref.mat Envelope EnvelopeF
    %RecLLTs = RecLLT(FminId:FmaxId);
    
    %Pecs = Hres.*EnvelopeF.'*Amp; % tak je to v recOAEprobe
    %Hres = Pecs.*RecLLTs.'./(0.5*k_env*Amp); % puvodni
    EnvelopeF = EnvelopeF(FminId:FmaxId);
    Hres = Pecs./(EnvelopeF.'*Amp);
    
    % else
    %     fx=fx(FminId:FmaxId);    
    %     Pecs_withoutR = Pecs;
    %     R = 1;
else
    % spl calibration
    
    if EarSim  % depth compensated ear simulator          
        % plot recorded
                
        
        fx = fxI(FminId:FmaxId);
        % interpolate to reach agreement between dim.
        LLTHres1 = interp1(LLT.fx,LLT.Hres1,fx,'spline');
        LLTHres2 = interp1(LLT.fx,LLT.Hres2,fx,'spline');
        % plot figures
        figure(22);
        subplot(3,1,1);
        hold on;
        plot(fx,abs(Hres1./LLTHres1))
        plot(fx,abs(Hres2./LLTHres2))
        ylabel('Transfer function vs LLT (-)')
        xlim([2e3 10e3]);  % for detailed region of lambda/2 resonance
%         xlim([100 20e3]);
        hold off;
        subplot(3,1,2);
        hold on;
        semilogx(fx,20*log10(abs(Hres1)))
        semilogx(fx,20*log10(abs(Hres2)))
%         xlim([2e3 10e3]);  % for detailed region of lambda/2 resonance
        xlim([100 20e3]);
        hold off;
        
        ylabel('Transfer function (dB)')
        subplot(3,1,3);
        hold on;
        semilogx(fx,20*log10(abs(Pecs1/(sqrt(2)*2e-5))))
        semilogx(fx,20*log10(abs(Pecs2/(sqrt(2)*2e-5))))
%         xlim([2e3 10e3]);  % for detailed region of lambda/2 resonance
        xlim([100 20e3]);
        hold off;
        xlabel('Frequency (Hz)')
        ylabel('Pressure response (dB SPL)')
        uiwait(msgbox('choose calibration file',''));
        
        [filename, pathname] = uigetfile('Calibration/');
               
        load([pathname filename]);
        
        fx = EarSim.fxI;
        
        Lpist = 124; % pistonphone
        rmspist = 0.0753; % recorded pist response in MU for 0 dB amplification in sound card
        
        BKmicSens = rmspist./(2e-5*10^(Lpist/20));
        Lreq = 124;
        
        
        Hres1 = EarSim.HresSim1./(BKmicSens*GainMicA);
        Hres2 = EarSim.HresSim2./(BKmicSens*GainMicA);
        
        
        FminId = find(fx>=Fmin,1,'first');
        FmaxId = find(fx>=Fmax,1,'first');
        Hres1 = Hres1(FminId:FmaxId);
        Hres2 = Hres2(FminId:FmaxId);
        fx = fx(FminId:FmaxId);
        
    else
        % cast pro kalibraci in situ v uchu
        fx = fxI(FminId:FmaxId);
        
        % plot figures
        figure(22);
        subplot(2,1,1);
        box on;
        hold on;
        semilogx(fx,20*log10(abs(Hres1)))
        semilogx(fx,20*log10(abs(Hres2)))
        set(gca,'xscale','log');
%         xlim([2e3 10e3]);  % for detailed region of lambda/2 resonance
        xlim([100 20e3]);
        hold off;
        
        ylabel('Transfer function (dB)')
        LLTHres1 = interp1(LLT.fx,LLT.Hres1,fx,'spline');
        LLTHres2 = interp1(LLT.fx,LLT.Hres2,fx,'spline');
        % plot figures
        
        subplot(2,1,2);
        box on;
        hold on;
        plot(fx,abs(Hres1./LLTHres1))
        plot(fx,abs(Hres2./LLTHres2))
        set(gca,'xscale','log');
        %subplot(2,1,2);
%         hold on;
%         box on;
%         semilogx(fx,20*log10(abs(Pecs1/(sqrt(2)*2e-5))))
%         semilogx(fx,20*log10(abs(Pecs2/(sqrt(2)*2e-5))))
%         set(gca,'xscale','log');
%         xlim([2e3 10e3]);  % for detailed region of lambda/2 resonance
        xlim([100 20e3]);
        ylim([0 5]);
        hold off;
        xlabel('Frequency (Hz)');
        ylabel('Tr.Func/LLT (-)');
        %ylabel('Pressure response (dB SPL)')
        
        
        
    end
end
    



% calibration via EPL .. in progress

% OAE Calibration - post hoc Calibration of emissions        

if (O_EPL==1 && FPL==1)
        Rs = (Z0.' - Zsurge)./(Z0.' + Zsurge);
        %load Calibration\recInEar.mat
        PecU=Pecs;
        load Calibration\recTubeL.mat Pec
        Pec = Pec(FminId:FmaxId);
        PecX = PecU./Pec;

        %finding f/2 for selected range 1000 - 15000 kHz
        %fx = linspace(0,fsamp,8192);
        %fx = (0:length(Pec)-1)*fsamp/length(Pec);
        Fmin_peak = 1000;
        Fmax_peak = 14000;
        FminId_peak = find(fx>=Fmin_peak,1,'first');
        FmaxId_peak = find(fx>=Fmax_peak,1,'first');
        
        fx_peak = fx(FminId_peak:FmaxId_peak);
        PecX_peak = PecX(FminId_peak:FmaxId_peak);
        [Peak,In] = max(abs(PecX_peak));

        tau =1/(fx_peak(In));
        
        T = exp(-1*sqrt(-1)*2*pi()*fx*tau);

        %Pecs_withoutR = Pecs;
        Coae = (1-R.*Rs)./(T.*(1+Rs)); %emited pressure conv func
else
    
    Coae = 1;
    
    
end


save([Folder '\rec_' SubjName '_' YearS MonthS DayS '_' HourS MinuteS SecondS ...
    'recEarResp.mat'],'Hres1','Hres2','fx','Amp');



% save Calibration/inEarCalibration.mat Coae Pecs1 Pecs2 AmpCh fx Hres1 Hres2
save Calibration/inEarCalibration.mat Coae AmpCh fx Hres1 Hres2