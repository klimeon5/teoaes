function [CalSpect,fxI] = SpectrumToPascals(Spectrum,fxS,GainMicA)
% load DPOAE data and process (continous dpoae)

%fs = 96e3;


% fxI = 200:1:15e3; % interpolated freq axis
id1 = find(~isnan(Spectrum),1,'first'); % first not NaN value
id2 = find(~isnan(Spectrum),1,'last'); % last not NaN value
fxI = fxS(id1):1:fxS(id2); % interpolated freq axis

% check wheter is fxI empty
if isempty(fxI)
    fxI = NaN;
    CalSpect = NaN;
else
    [SSi] = interp1(fxS(id1:id2),Spectrum(id1:id2),fxI,'pchip'); % interpolate
    
    % convert to dB SPL
    
    %load Calibration/MicRefSensit.mat Hoaemicsens;
    load Calibration/CalibMicData.mat Hoaemicsens fx; % load Hoaemicsens (microphone sensitivity
    
    %fx = (0:length(Hoaemicsens)-1)*fs/length(Hoaemicsens); % frequency axis
    
    
    CalCurve = smooth(real(Hoaemicsens),50,'loess')+sqrt(-1)*smooth(imag(Hoaemicsens),50,'loess');
    CalCurveI = interp1(fx,real(CalCurve),fxI,'pchip') + sqrt(-1)*interp1(fx,imag(CalCurve),fxI,'pchip');
    
    %f = f*1000;
    %CalCurve= smooth(abs(2*GainMicA*Href*RefSens),50,'loess'); % smooth the noise out
    %CalCurve= smooth(abs(),50,'loess'); % smooth the noise out
    
    %[CalCurveI] = interp1(fx,CalCurve,fxI,'pchip'); % interpolate
    
    CalSpect = SSi./(CalCurveI*GainMicA);
    
    
    
    % calibration to turn the measured data into dB SPL
end
    
    
    
    % figure;
    % hold on;
    % plot(fx,20*log10(abs(fft(IntWins)/Nsamp)));
    % plot(fx,20*log10(abs(fft(IntWSmart)/Nsamp)));
    
    
    
    %
    %
    %
    % for k=1:opak
    %
    %     ExtWin = nahravka(Nonset+(k-1)*Nsamp:k*Nsamp+Nonset);
    %     IntWin = IntWin+ExtWin;
    %
% end