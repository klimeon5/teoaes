function [CalSpect,fxI] = getRecSOAESpectrum(nahravka,fsamp,GainMicA)
% calculate spectrum from recorded signal for SOAE detection


Nframe = fsamp; % one second long frames
Nframes = floor(length(nahravka)/Nframe);
Ntotal = Nframes*Nframe; % total length of used samples


fcutoff =100; % cutoff freq of 2order high pass butterworth filter
[b,a] = butter(4,fcutoff/(fsamp/2),'high');
RSfilted = filtfilt(b,a,nahravka);
    
RSsel = RSfilted(1:Ntotal);

RSsel = reshape(RSsel,Nframe,Nframes);
RSsel2 = RSsel(:,20*log10(sqrt(mean(RSsel.^2))./(2e-5)) < 30);
SRSsel = fft(RSsel2,Nframe)/Nframe;

Med = median(abs(SRSsel.'));

fx = (0:Nframe-1)*fsamp/Nframe;

% figure;
% hold on;
% plot(fx,abs(Med));

SpectSmart = Med;

fxI = 100:1:20e3;

[SSi] = interp1(fx,abs(SpectSmart),fxI,'pchip'); % interpolate

% convert to dB SPL

%load Calibration/MicRefSensit.mat Hoaemicsens; 
% load Calibration/CalibMicData.mat Hoaemicsens fx; % load Hoaemicsens (microphone sensitivity
load Calibration/CalibMicDataSDist16mmYellowFoam13mm.mat Hoaemicsens fx; % load Hoaemicsens (microphone sensitivity

%fx = (0:length(Hoaemicsens)-1)*fs/length(Hoaemicsens); % frequency axis


CalCurve = smooth(real(Hoaemicsens),50,'loess')+sqrt(-1)*smooth(imag(Hoaemicsens),50,'loess');
CalCurveI = interp1(fx,real(CalCurve),fxI,'pchip') + sqrt(-1)*interp1(fx,imag(CalCurve),fxI,'pchip');

%f = f*1000;
%CalCurve= smooth(abs(2*GainMicA*Href*RefSens),50,'loess'); % smooth the noise out
%CalCurve= smooth(abs(),50,'loess'); % smooth the noise out

%[CalCurveI] = interp1(fx,CalCurve,fxI,'pchip'); % interpolate

CalSpect = 2*SSi./(CalCurveI*GainMicA);

