function [Teoae_time_zeros, Teoae, fxI] = getRecTEOAESpectrum(fs,fx,Rsh,Hoaemicsens,IndTeoaeStart,tau)


% %% Artifact rejection procedure according to Yi-Wen Liu
% % A point-wise artifact rejection method for estimating
% % transient-evoked otoacoustic emissions and their group delay
% 
% %  Frame exclusion
% % Roae = Rsh(550:1700,:); %doresit


Roae = Rsh(IndTeoaeStart:1700,:); %doresit
% Roae = Rsh;

% [MaxRsh,IndMaxRsh] = max((nanmean(Rsh')));
% tau=(IndMaxRsh-IndTeoaeStart)/fs; %latency between click and TEOAE
% [idxx, idxy] = find(abs(Roae) > 2e-3);
% for ii=1:length(idxx)
%     Roae(idxx(ii),idxy(ii))=NaN;
% end
RoaeMean = nanmean(Roae.');

%  Noise shaping
noiseMat=(Roae-RoaeMean')';
Roae=Roae';
% figure;histogram(noiseMat(801,:));
numSamples=length(noiseMat(1,:));
sigma=sqrt(nanmean(mean(noiseMat'.^2)));
% sigma=sqrt(nanmean(nanmean(noiseMat'.^2)));
Theta=2*sigma;

[idxx, idxy] = find(abs(noiseMat) > Theta);
for ii=1:length(idxx)
    noiseMat(idxx(ii),idxy(ii))=NaN;
    Roae(idxx(ii),idxy(ii))=NaN;
end

Teoae_time=nanmean(Roae);

% Kolmogorov-Smirnof test
% hV=zeros(1,numSamples);
% pV=zeros(1,numSamples);
% for ii=1:numSamples
%     [h,p] = kstest(noiseMat(:,ii));
%     hV(ii)=h;
%     pV(ii)=p;
% end


% Teoae_time_zeros = [Teoae_time, zeros(1,1e4)];
% Teoae_time_zeros = [Teoae_time.*tukeywin(length(Teoae_time),0.1)', zeros(1,1e4)];
Teoae_time_zeros = [zeros(1,fs*abs(tau)), Teoae_time(1:end-400).*tukeywin(length(Teoae_time(1:end-400)),0.1)', zeros(1,1e4)];

fxI = (0:length(Teoae_time_zeros)-1)*fs/length(Teoae_time_zeros); % frequency axis
% figure; plot(fxI,abs(fft(Teoae_time_zeros)))

HoaemicsensX = interp1(fx,Hoaemicsens,fxI,'spline');
Teoae=fft(Teoae_time_zeros)/length(Teoae_time_zeros)./HoaemicsensX;