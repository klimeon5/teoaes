% close all;
clear all;

filenameList={...
% 'TEOAE_s007_220721_101060_L_60_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ...    
% 'TEOAE_s007_220721_101517_L_70_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ...    
% 'TEOAE_s007_220721_101824_L_80_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ...  
% 'TEOAE_s007_220721_102130_L_90_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ... 
% 'TEOAE_s007_220721_102434_L_100_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ... 
% 'TEOAE_s007_220721_102913_L_60_WinClickDur_50_NumOfPresentedClicks_3000__Left.mat', ...     
% 'TEOAE_s007_220721_103214_L_70_WinClickDur_50_NumOfPresentedClicks_3000__Left.mat', ...     
% 'TEOAE_s007_220721_103518_L_80_WinClickDur_50_NumOfPresentedClicks_3000__Left.mat', ...     
% 'TEOAE_s007_220721_103821_L_90_WinClickDur_50_NumOfPresentedClicks_3000__Left.mat', ...     
% 'TEOAE_s007_220721_104118_L_100_WinClickDur_50_NumOfPresentedClicks_3000__Left.mat', ...  

'TEOAE_s003_220714_152344_L_60_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ...      
'TEOAE_s003_220714_154106_L_70_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ...      
'TEOAE_s003_220714_153734_L_80_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ...      
'TEOAE_s003_220714_153427_L_90_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ...      
'TEOAE_s003_220714_153006_L_100_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ...     
'TEOAE_s003_220714_152703_L_110_WinClickDur_50_NumOfPresentedClicks_3000__Right.mat', ...     



};


%deltaF = 20; % frequency step
build = 1;

if build ==1
    
sampleF2 = 3e3;
uPa = 1e6;
L2var = 30:5:60;
Lvar = 30:5:60;
Lvar = 30;
plotflag = 0;

parW.nmm=25;
parW.nb=6; % n. 1/3 oct bands
parW.tt=13; % latency power law: tt*f(kHz)^-bbb
parW.bbb=1;
parW.p0=2e-5;
parW.tc=-50;
parW.tch=50;
parW.c1=-0.5;
parW.c2=0.5;
parW.c3=1.;
parW.c4=2.5;
parW.c5=3;

plotflag =1;


% for k=1:length(L2var)
%     %load(['Results\s003_24_04_pouze_sweepy\DPgrams\dpgL1_' num2str(Lvar(k)) 'L2_50.mat']);
%     load(['..\Results\s003\r11\DPgrams\dpgL1_50L2_' num2str(L2var(k)) '.mat']);
%     %load(['Results\s001_sweepy_f2f1_120_22_5_2019\DPgrams\dpgL1_50L2_' num2str(Lvar(k)) '.mat']);
%     %load(['Results\s001_sweepy_f2f1_120_22_5_2019\DPgrams\dpgL1_' num2str(Lvar(k)) 'L2_50.mat']);
% 
% 
%     [fx, DPgAlla, DPgNLa, DPgCRa, DPgCR1a, DPgAllreca, DPgAllph, DPgNLph, DPgCRph, DPgCR1ph, DPgAllrecph, ...
%         fc, DPtoctAlla, DPtoctNLa, DPtoctCRa] = WLTdecompFCE(parW,freqf2,DPgram,plotflag);
% 
%     idxSample = find(fx>=sampleF2,1,'first'); % index where to take the signal
% 
%    PnlAL1(k,:) = DPgNLa;
%     PnlPhL1(k,:) = DPgNLph;
% 
%     PcrAL1(k,:) = DPgCRa;
%     PcrPhL1(k,:) = DPgCRph;
% 
% end



% L1var = 50:1:60;
%Lvar = 30:5:60;
%Lvar = 30;
plotflag = 1;
cmList = [5.6571   16.1390   44.3643  113.8800  256.6645]; %s007

for k=1:length(filenameList)
    
%     lf=strjoin(['D:\Experimenty\oaes_v2\Results\s007\TEOAE\bez_kompresni_metody\','Spec',filenameList(k)],'');
    lf=strjoin(['D:\Experimenty\oaes_v2\Results\s003\TEOAE\','Spec',filenameList(k)],'');
%     lf=strjoin(['D:\Experimenty\oaes_v2\Results\s007\TEOAE\kompresni_metoda\','Spec',filenameList(k)],'');
    % lf=strjoin(['D:\Experimenty\oaes_v2\Results\trubicka\TEOAE\',filenameList(k)],'');
    load(lf); %clear hObject
    
%     load(['..\Results\s003\DPOAEnotchMOH\r111\DPgrams\dpgL1_' num2str(L1var(k)) 'L2_50.mat']);
    %load(['Results\s003_24_04_pouze_sweepy\DPgrams\dpgL1_50L2_' num2str(L2var(k)) '.mat']);
    %load(['Results\s001_sweepy_f2f1_120_22_5_2019\DPgrams\dpgL1_50L2_' num2str(Lvar(k)) '.mat']);
    %load(['Results\s001_sweepy_f2f1_120_22_5_2019\DPgrams\dpgL1_' num2str(Lvar(k)) 'L2_50.mat']);
% fxI=[7,fxI];

fxIEx=linspace(fxI(29),fxI(1024),4096);
TeoaeEx = interp1(fxI(29:1024),Teoae(29:1024),fxIEx,'linear');
% TeoaeEx = interp1(fxI,Teoae(29:541),fxIEx,'linear','extrap');
    
TeoaeEx=tukeywin(length(TeoaeEx),0.3).*TeoaeEx.';
[fx, DPgAlla, DPgNLa, DPgCRa, DPgCR1a, DPgAllreca, DPgAllph, DPgNLph, DPgCRph, DPgCR1ph, DPgAllrecph, ...
        fc, DPtoctAlla, DPtoctNLa, DPtoctCRa,cm] = WLTdecompFCE_TEOAE(parW,fxIEx,TeoaeEx,plotflag,cmList,k);
% cmList(k)=cm;
    idxSample = find(fx>=sampleF2,1,'first'); % index where to take the signal
    %pause;
     PnlAL2(k,:) = DPgNLa;
    PnlPhL2(k,:) = DPgNLph;

    PcrAL2(k,:) = DPgCRa;
    PcrPhL2(k,:) = DPgCRph;
    
%     PnlAL2(k) = DPtoctNLa(4);
%     
%     PcrAL2(k) = DPtoctCRa(4);
%     
    

end
end

% figure; 
% subplot(2,2,1)
% surf(fx,L2var,PnlAL1); shading flat; view([0 90]);
% title('P.C.')
% xlabel('f2 (Hz)')
% ylabel('L2 (dB)')
% set(gca,'clim',[-10 15]);
% colorbar;
% 
% subplot(2,2,2)
% surf(fx,L2var,PnlAL2); shading flat; view([0 90]);
% title('P.C.')
% xlabel('f2 (Hz)')
% ylabel('L1 (dB)')
% set(gca,'clim',[-10 15]);
% colorbar;
% 
% 
% subplot(2,2,3)
% surf(fx,L2var,PcrAL1); shading flat; view([0 90]);
% title('S.C.')
% xlabel('f2 (Hz)')
% ylabel('L2 (dB)')
% set(gca,'clim',[-10 15]);
% colorbar;
% 
% subplot(2,2,4)
% surf(fx,L2var,PcrAL2); shading flat; view([0 90]);
% title('S.C.')
% xlabel('f2 (Hz)')
% ylabel('L1 (dB)')
% set(gca,'clim',[-10 15]);
% colorbar;
% 
% 














% 
% % 
% % figure;
% % subplot(2,1,1);
% % plot(Lvar,PnlA,Lvar,PcrA);
% % subplot(2,1,2);
% % plot(Lvar,(unwrap(PnlPh)-PnlPh(1))/cycle,Lvar,(unwrap(PcrPh)-PcrPh(1))/cycle);
% 
% 
% 
% figure;
% 
% set(gcf, 'Units', 'centimeters');
% % we set the position and dimension of the figure ON THE SCREEN
% %
% % NOTE: measurement units refer to the previous settings!
% SCALE = 1.5;
% afFigurePosition = [1 1 18 13];
% % [pos_x pos_y width_x width_y]
% set(gcf, 'Position', afFigurePosition); % [left bottom width height]
% % we link the dimension of the figure ON THE PAPER in such a way that
% % it is equal to the dimension on the screen
% %
% % ATTENTION: if PaperPositionMode is not ’auto’ the saved file
% % could have different dimensions from the one shown on the screen!
% set(gcf, 'PaperPositionMode', 'auto');
% 
% iFontSize = 11;
% strFontName = 'Helvetica';
% LWtick = 1.8;
% LW = 1.3;
% 
% leftCornerX = 0.1;
% leftCornerY = 0.52;
% wEnv = 0.38;
% hEnv = 0.41;
% xD = 0.06
% rampdur = 10;% duration of ramp of F2 tone
% tPulseOn = 10; % time when the ramp is turned on;
% tPulseOff = 30; % time when the ramp is turned off;
% 
% % Freq limitx of X axis
% 
% Xlim = [20 70];
% YlimA = [0 40];
% YlimPh = [-0.15 0.09];
% 
% %% first column
% 
% ha1 = axes('position',[leftCornerX leftCornerY  wEnv hEnv],'xscale','lin','yscale','lin','nextplot','add');
% l1 = plot(L2var,uPa*PnlAL1,'linewidth',LW,'color',[0 0 0]);
% l2 = plot(L2var,uPa*PcrAL1,'linewidth',LW,'color',[.6 .6 .6]);
% %l2 = plot(L2,abs(PP2Ab)*1e6,'linewidth',LW,'color',[.6 .6 .6]);
% 
% 
% 
% %plot(JMG1.F1vect,20*log10(JMG1.AmpOut/P0),'linewidth',LW,'color',[.6 .6 .6])
% %plot(CompG1.F1vect,20*log10(CompG1.AmpOut/P0),'linewidth',LW,'color',[0 0 0])
% %plot(JMG1.F1vect,20*log10(JMG1.AmpOut/P0),'linewidth',LW,'color',[.6 .6 .6])
% 
% set(ha1,...
%     'xlim',Xlim,...
%     'ylim',YlimA,...
%     'box','on',...
%     'fontsize',iFontSize,...
%     'xticklabel','',...
%     'fontname',strFontName...
%    );
% 
% ylabel('DPOAE amplitude (\muPa)','fontsize',iFontSize+1,'fontname',strFontName,'fontweight','normal')
% 
% text(20,max(YlimA)*1.06,['{\itL}_1 = 50 dB FPL, {\itF}_2 = ' num2str(sampleF2/1e3) ' kHz'],'fontsize',iFontSize+1,'fontname',strFontName,'fontweight','normal',...
%    'horizontalalignment','left','rotation',0)
% 
% %text(0.22,0,'Amplitude difference (\muPa)','fontsize',iFontSize+2,'fontname',strFontName,'fontweight','normal',...
% %    'horizontalalignment','center','rotation',90)
% 
% %text(0.8,-320,'Frequency (kHz)','fontsize',iFontSize+2,'fontname',strFontName,'fontweight','normal',...
% %    'horizontalalignment','center','rotation',0)
% cycle = 2*pi;
% 
% hph1 = axes('position',[leftCornerX leftCornerY-hEnv  wEnv hEnv],'xscale','lin','yscale','lin','nextplot','add');
% l1 = plot(L2var,(unwrap(PnlPhL1)-PnlPhL1(1))/cycle,'linewidth',LW,'color',[0 0 0]);
% l2 = plot(L2var,(unwrap(PcrPhL1)-PcrPhL1(1))/cycle,'linewidth',LW,'color',[.6 .6 .6]);
% 
% %plot(JMG1.F1vect,20*log10(JMG1.AmpOut/P0),'linewidth',LW,'color',[.6 .6 .6])
% %plot(CompG1.F1vect,20*log10(CompG1.AmpOut/P0),'linewidth',LW,'color',[0 0 0])
% %plot(JMG1.F1vect,20*log10(JMG1.AmpOut/P0),'linewidth',LW,'color',[.6 .6 .6])
% 
% set(hph1,...
%     'xlim',Xlim,...
%     'ylim',YlimPh,...
%     'box','on',...
%     'fontsize',iFontSize,...
%     'fontname',strFontName...
%    );
% 
% ylabel('DPOAE phase (cycles)','fontsize',iFontSize+1,'fontname',strFontName,'fontweight','normal')
% 
% xlabel('{\itL}_2 (dB FPL)','fontsize',iFontSize+1,'fontname',strFontName,'fontweight','normal')
% cycle = 2*pi;
% 
% %load DPcomponentsS003L2_2000Hz.mat % s003, L1 konstant
% 
% ha2 = axes('position',[leftCornerX+hEnv+xD leftCornerY  wEnv hEnv],'xscale','lin','yscale','lin','nextplot','add');
% l1 = plot(L1var,uPa*PnlAL2,'linewidth',LW,'color',[0 0 0]);
% l2 = plot(L1var,uPa*PcrAL2,'linewidth',LW,'color',[0.6 0.6 0.6]);
% %l2 = plot(L1,abs(PP2Ab)*1e6,'linewidth',LW,'color',[.6 .6 .6]);
% 
% Xlim = [30 80];
% YlimA = [0 120];
% YlimPh = [-1.0 0.1]
% 
% set(ha2,...
%     'xlim',Xlim,...
%     'ylim',YlimA,...
%     'box','on'  ,...
%     'fontsize',iFontSize,...
%     'xticklabel','',...
%     'fontname',strFontName...
%    );
% 
% % 
% % legend([l1 l2],{'Primary','Secondary'},'position',[leftCornerX+hEnv-0.2+hEnv+xD leftCornerY+0.01 0.2 0.1],...
% %    'fontsize',iFontSize,'fontname',strFontName,'orientation','vertical');
% 
% 
% 
% text(30,max(YlimA)*1.06,['{\itL}_2 = 50 dB FPL, {\itF}_2 = ' num2str(sampleF2/1e3) ' kHz'],'fontsize',iFontSize+1,'fontname',strFontName,'fontweight','normal',...
%    'horizontalalignment','left','rotation',0)
% 
% 
% hph2 = axes('position',[leftCornerX+hEnv+xD leftCornerY-hEnv  wEnv hEnv],'xscale','lin','yscale','lin','nextplot','add');
% l1 = plot(L1var,(unwrap(PnlPhL2)-PnlPhL2(1))/cycle,'linewidth',LW,'color',[0 0 0]);
% l2 = plot(L1var,(unwrap(PcrPhL2)-PcrPhL2(1))/cycle,'linewidth',LW,'color',[.6 .6 .6]);
% %l2 = plot(L1l,unwrap(RcmpPh)/(2*pi),'linewidth',LW,'color',[.6 .6 .6]);
% 
% 
% 
% %plot(JMG1.F1vect,20*log10(JMG1.AmpOut/P0),'linewidth',LW,'color',[.6 .6 .6])
% %plot(CompG1.F1vect,20*log10(CompG1.AmpOut/P0),'linewidth',LW,'color',[0 0 0])
% %plot(JMG1.F1vect,20*log10(JMG1.AmpOut/P0),'linewidth',LW,'color',[.6 .6 .6])
% 
% set(hph2,...
%     'xlim',Xlim,...
%     'ylim',YlimPh,...
%     'box','on',...
%     'fontsize',iFontSize,...
%     'fontname',strFontName...
%    );
% 
% 
% %legend([l1 l2],{'diff bewteen primary and secondary comp.'},'position',[leftCornerX+hEnv-0.2+hEnv+xD leftCornerY+0.01 0.2 0.1],...
% %   'fontsize',iFontSize,'fontname',strFontName,'orientation','vertical');
% 
% 
% xlabel('{\itL}_1 (dB FPL)','fontsize',iFontSize+1,'fontname',strFontName,'fontweight','normal')
% 
% % text(4.7e3,36,'Compression','fontsize',iFontSize,'fontname',strFontName,'fontweight','bold',...
% %     'horizontalalignment','center','rotation',0)
% % 
% % 
% % text(6.2e3,22,'A1-1','fontsize',iFontSize,'fontname',strFontName,'fontweight','bold',...
% %     'horizontalalignment','right','rotation',0)
% 
% % 
% iResolution = 300;
% strFileName = ['ResultsSC19/Figures/DPcompS003r12_F2_' num2str(sampleF2) 'Hz'];
% print('-depsc',  strcat(strFileName, '.eps'));
% 
