function [click] = genCEOAEclick(Lspl1,fsamp,PulseWinLength,ClickCalibType)
% generate Two logarithmic synchronized swept sines and set their level for DPOAE measurement
% fsamp = 44.1e3; % sampling frequency
% f2b = 2000; % end frequency of f1 tone
% f2e = 4000; % end frequency of f2 tone
% f2f1 = 1.2; % f2/f1 ratio
% Rdur = 0.02; % ramp duration
% octpersec = 0.5;
% phi1s = 0; % starting freq
% phi2s = pi; % starting freq
% Lspl2 = 50;
% Lspl1 = 50;



% Noct = log2(f2e/f2b); % give number of octaves

% PulseWinLength = 50 * 1e-3;

switch ClickCalibType
    case 1 %standard click calibration
        
        tx = (0:1/fsamp:PulseWinLength - 1/fsamp)';
        
        % f1b = f2b/f2f1;
        % f1e = f2e/f2f1;
        
        % y2 = zeros(length(tx),1);
        % y1 = zeros(length(tx),1);
        % InstFreq2 = zeros(size(y2));
        % InstFreq1 = zeros(size(y1));
        
        PulseOrDirac='Pulse';
        PulseOrDirac='Dirac';
        
        switch PulseOrDirac
            case 'Pulse'
                y1=[zeros(1,ceil(length(tx)*2/10.7)), ones(1,ceil(length(tx)*0.1/10.7))];
                y1=[y1,zeros(1,length(tx)-length(y1))];
            case 'Dirac'
                y1=[zeros(1,ceil(length(tx)*2/10.7)), ones(1,1)];
                y1=[y1,zeros(1,length(tx)-length(y1))];
        end
        fcutoff =500; % cutoff freq of 2order high pass butterworth filter
        [b,a] = butter(2,[500/(fsamp/2) 5e3/(fsamp/2)],'bandpass');
        %     [b,a] = butter(4,[100/(fsamp/2) 20e3/(fsamp/2)],'bandpass');
        %     [b,a] = butter(4,20e3/(fsamp/2),'low');
        
        %     ButterFiltSig = filter(b,a,y1);
        %     ZeroPhaseFiltSig = filtfilt(b,a,y1);
        ZeroPhaseFiltSig = y1;
        
        %y1=ifft((fft(y1)./abs(fft(y1))));
        
        % % f2 tone
        % gamma2 = T/log(f2e/f2b);
        % y2 = sin(2*pi*f2b*gamma2*exp(tx/gamma2));
        % InstFreq2 = f2b*exp(tx/gamma2);
        %
        % % f1 tone
        % gamma1 = T/log(f1e/f1b);
        % y1 = sin(2*pi*f1b*gamma1*exp(tx/gamma1));
        % InstFreq1 = f1b*exp(tx/gamma1);
        
        % set needed amplitude for both tones
        
        load Calibration/inEarCalibration.mat  AmpCh fx Hres1 Hres2
        
        % RecEarInAbs = smooth(abs(Pecs),30,'loess');
        HresS1 = smooth(abs(Hres1),30,'loess');
        
        % HresS2 = smooth(abs(Hres2),30,'loess');
        %fs = 96e3;
        
        %fxA = (0:length(RecEarInAbs)-1)*fs/length(RecEarInAbs);
        
        fxA=fx;
        Hres1Inv = 1./Hres1;
        Hcut = Hres1Inv(60:500);  % cut LF part which is too high (high DC)
        %Hcutall = interp1([fxA(1) fxA(60:end)],[0 Hcut],fxA,'spline');
        
        % [bb,aa] = invfreqz(Hcut,fxA(60:500)/fsamp*2*pi,4,5);
        
        % ButterFiltSigH = filter(bb,aa,ButterFiltSig);
        % ZeroPhaseFiltSigH = filtfilt(bb,aa,ZeroPhaseFiltSig);
        
        click=ZeroPhaseFiltSig;
        [click,peSPL]=peSPLScale(click);
    case 2 %calibration of click using CGLS
        load('D:\Experimenty\oaes_v2\Calibration\CGLS_reconst_click_long_tube.mat');
        [clickResp,peSPL]=peSPLScale(DiracResp);
        %         20*log10(peSPL.pp/2e-5);
        click=Stimulus'/peSPL.pp;
        click=click.*tukeywin(length(click),0.35)';
        click=[click,zeros(1,PulseWinLength*fs-length(click))];
        

end
click = click .* 10.^(Lspl1/20)*2e-5;



return



scale1 = AmpCh*10.^((Lspl1-20*log10(Lx1/(sqrt(2)*2e-5)))/20);
scale1H = 10.^(Lspl1/20)*sqrt(2)*2e-5./Lx1H;

scale1H = 10.^(Lspl1/20)*2*2e-5./Lx1H;

% [Lx2] = interp1(fxA,RecEarInAbs,InstFreq2.','pchip');
% [Lx1] = interp1(fxA,RecEarInAbs,InstFreq1.','pchip');

% [Lx2H] = interp1(fxA,HresS2,InstFreq2,'pchip');
[Lx1H] = interp1(fxA,HresS1,InstFreq1,'pchip');

%idxF = find(fx==freq);

% scale2 = AmpCh*10.^((Lspl2-20*log10(Lx2/(sqrt(2)*2e-5)))/20);
% scale1 = AmpCh*10.^((Lspl1-20*log10(Lx1/(sqrt(2)*2e-5)))/20);

% scale2H = 10.^(Lspl2/20)*sqrt(2)*2e-5./Lx2H;
% scale1H = 10.^(Lspl1/20)*sqrt(2)*2e-5./Lx1H;

scale1H = 10.^(Lspl1/20)*2*2e-5./Lx1H;

% y2scaled = scale2.*y2;
% y1scaled = scale1.*y1;

% y2scaled = scale2H.*y2;
%return
y1scaled = scale1H.*y1;

% shape onset and offset

% % ramp
% x = [0:1/fsamp:Rdur]';
% x = pi*x/Rdur;
% rampUp = (1 + cos(x + pi))/2;
% rampDown = flipud(rampUp);
% Nsamp = length(tx);
%
% wholeramp = [rampUp; ones(Nsamp-2*length(x),1); rampDown];
%
% %y2 = wholeramp;
%
% y1ramped = wholeramp.*y1scaled;
%
% y2ramped = wholeramp.*y2scaled;


