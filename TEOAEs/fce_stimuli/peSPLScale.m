function [click,peSPL] = peSPLScale(click)
peSPL.max=max(click);
peSPL.min=min(click);
peSPL.bp=max(abs([peSPL.min peSPL.max]));
peSPL.pp=(peSPL.max-peSPL.min);
click=click./peSPL.pp;
end