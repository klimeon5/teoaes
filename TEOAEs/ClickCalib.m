fs=96000;
fsamp=fs;
PulseWinLength=0.05;
WinClickDur=1000*PulseWinLength;
WhichEar='T';
tx = (0:1/fs:PulseWinLength - 1/fs)';
L1 = 100;

y1=1*[zeros(1,ceil(length(tx)*2/10.7)), ones(1,1)];
y1=1*[ones(1,1)];
CEOAEBlock=[y1,zeros(1,length(tx)-length(y1))];



fcutoff =100; % cutoff freq of 2order high pass butterworth filter
[b,Roae] = butter(2,[1500,4000]/(fs/2));
% RSfilted = filter(b,Roae,recordedSignal(:,1));
% RSfilted = filter(b,Roae,recordedSignal);
pokusfiltr = filtfilt(b,Roae,CEOAEBlock);

%%

load('D:\Experimenty\oaes_v2\Results\trubka\gauss_TEOAE_long_lossy_tube_230125_155001_WinClickDur_50000_NumOfPresentedClicks_8000_T_1.mat')
% load('D:\Experimenty\oaes_v2\Results\trubka\TEOAE_long_lossy_tube_230124_113159_WinClickDur_50000_NumOfPresentedClicks_8000_T_1.mat')
% load('D:\Experimenty\oaes_v2\Results\trubka\bandpass_TEOAE_long_lossy_tube_230125_154121_WinClickDur_50000_NumOfPresentedClicks_8000_T_1.mat')
% load('D:\Experimenty\oaes_v2\Results\trubka\reconst_TEOAE_long_lossy_tube_230125_180840_WinClickDur_50000_NumOfPresentedClicks_8000_T_1.mat')

% load('D:\Experimenty\oaes_v2\Results\trubka\reconst_TEOAE_long_lossy_tube_230127_103516_WinClickDur_50000_NumOfPresentedClicks_8000_T_1.mat')

% load('D:\Experimenty\oaes_v2\Results\trubka\strikacka_TEOAE_long_lossy_tube_230127_124920_WinClickDur_50000_NumOfPresentedClicks_8000_T_1.mat')
% load('D:\Experimenty\oaes_v2\Results\trubka\ucho_TEOAE_long_lossy_tube_230201_102236_WinClickDur_50000_NumOfPresentedClicks_8000_T_1.mat')


WholerecordedSignal = recordedSignal(3141:end,1);
fcutoff =100; % cutoff freq of 2order high pass butterworth filter
[b,Roae] = butter(4,fcutoff/(fs/2),'high');
% RSfilted = filter(b,Roae,recordedSignal(:,1));
% RSfilted = filter(b,Roae,recordedSignal);
RSfilted = filtfilt(b,Roae,WholerecordedSignal);
% RSf = RSfilted(3141:end);
RSf = RSfilted;
NumOfClicks=floor(length(RSf)/length(CEOAEBlock));
RSf = RSf(1:NumOfClicks*length(CEOAEBlock));
Rsh = reshape(RSf,length(CEOAEBlock),NumOfClicks);
DiracResp=mean(Rsh.');
%%
DiracResp=DiracResp(450*fs/48000:end);
DiracResp=DiracResp(1029:end)*100;
%%
CEOAEBlockIndStart=find(CEOAEBlock,1,'first');
% CEOAEBlock=CEOAEBlock(CEOAEBlockIndStart:end);
% DiracResp=DiracResp(CEOAEBlockIndStart:end)*100;
%%
CEOAEBlock=CEOAEBlock(1:length(DiracResp));
% DiracResp=pokusfiltr; %%%% POKUS

DiracRespMat=zeros(length(DiracResp));
for ii=1:length(DiracResp)
    for jj=1:length(DiracResp)
        if jj<=ii
            DiracRespMat(ii,jj)=DiracResp(ii-jj+1);
        else
            DiracRespMat(ii,jj)=0;
        end
    end
end
addpath('regu/');
addpath('fce_functions/');
% [X,rho,eta,F] = cgls(DiracRespMat,CEOAEBlock',500,0,0);
[X,rho,eta,F] = cgls(DiracRespMat,CEOAEBlock',500,0,0);
ZZ1 = conv(DiracResp, X(:,end));
ZZ = conv(X(:,end),DiracResp);  

%  x2 = conjgrad(DiracRespMat,CEOAEBlock');
 x3 = cgs(DiracRespMat,CEOAEBlock');
 
