function RecPist = PPArec(buffersize,RecChanList,fsamp,Dur)
% use psych port audio to record signal from sound card


   
    clear PsychPortAudio; % clear portaudio (
    PsychPortAudio('Verbosity', 5); %
    %oldlevel = PsychPortAudio(�Verbosity� [,level]);
    %Set level of verbosity for error/warning/status messages. �level� optional, new
    %level of verbosity. �oldlevel� is the old level of verbosity. The following
    %levels are supported: 0 = Shut up. 1 = Print errors, 2 = Print also warnings, 3
    %= Print also some info, 4 = Print more useful info (default), >5 = Be very
    %verbose (mostly for debugging the driver itself).
    RecChanList = RecChanList ; %str2double(get(handles.recChan,'string'));
   
    
    mode = 2; % record mode (record)
    reqlatencyclass = 1; % latency class (0 - 3)
   % buffersize = 2048; % buffersize
    dev = PsychPortAudio('GetDevices'); % get list of all devices
    for k=1:length(dev), if contains(dev(k).DeviceName,'ASIO Fireface USB'), devID = k-1; end, end;
    %pahandle = PsychPortAudio('Open', devID, mode, reqlatencyclass, fsamp, length(RecChanList),buffersize,0.005,[RecChanList]);
%     pahandle = PsychPortAudio('Open', devID, mode, reqlatencyclass, fsamp, 1,buffersize,0.005,1);
    pamaster = PsychPortAudio('Open', devID, 2+8, reqlatencyclass, fsamp, max(RecChanList),buffersize,0.005);
    PsychPortAudio('Start', pamaster, 0, 0, 1);
    pahandle = PsychPortAudio('OpenSlave',pamaster,mode,length(RecChanList),RecChanList);
    %pahandle = PsychPortAudio(�Open� [, deviceid][, mode][, reqlatencyclass][, freq][, channels][, buffersize][, suggestedLatency][, selectchannels][, specialFlags=0]);
    
   % InputSig = [yChArr.'; 0*yChArr.'; yChArr.'];
    
    %PsychPortAudio('FillBuffer', pahandle, InputSig);
    %[underflow, nextSampleStartIndex, nextSampleETASecs] = PsychPortAudio(�FillBuffer�, pahandle, bufferdata [, streamingrefill=0][, startIndex=Append]);
%     Dur = 5;
    TimeDur = Dur;
    PsychPortAudio('GetAudioData', pahandle, TimeDur);
    %[audiodata absrecposition overflow cstarttime] = PsychPortAudio(�GetAudioData�, pahandle [, amountToAllocateSecs][, minimumAmountToReturnSecs][, maximumAmountToReturnSecs][, singleType=0]);
    PsychPortAudio('Start', pahandle,1);
    
%     WaitSecs(TimeDur/5); % pause to send data and receive signal
    WaitSecs(TimeDur);
    [input absrecposition overflow cstarttime] = PsychPortAudio('GetAudioData', pahandle);
    
    Status= PsychPortAudio('GetStatus', pahandle);
    
    [startTime endPositionSecs xruns estStopTime] = PsychPortAudio('Stop', pahandle, 1);
    %[startTime endPositionSecs xruns estStopTime] = PsychPortAudio(�Stop�, pahandle [, waitForEndOfPlayback=0] [, blockUntilStopped=1] [, repetitions] [, stopTime]);
    PsychPortAudio('Close');% Close the audio device:
    clear PsychPortAudio; % clear portaudio
    RecPist = input';
   
    if xruns>0||Status.XRuns
       fprintf("Under/overrun detected");
    end
