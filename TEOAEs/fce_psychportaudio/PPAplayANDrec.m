function RecChirp =  PPAplayANDrec(input,outputCh,inputCh,buffersize, fsamp)


    
    clear PsychPortAudio; % clear portaudio (
    PsychPortAudio('Verbosity', 5); %
    %oldlevel = PsychPortAudio(�Verbosity� [,level]);
    %Set level of verbosity for error/warning/status messages. �level� optional, new
    %level of verbosity. �oldlevel� is the old level of verbosity. The following
    %levels are supported: 0 = Shut up. 1 = Print errors, 2 = Print also warnings, 3
    %= Print also some info, 4 = Print more useful info (default), >5 = Be very
    %verbose (mostly for debugging the driver itself).
    RecChanList = [inputCh,5] ; %str2double(get(handles.recChan,'string'));
    PlayChanList = [outputCh,5];
    MasterChanPlay = max(PlayChanList);
    MasterChanRec = max(RecChanList);
    mode = 3; % duplex mode (play and record)
    reqlatencyclass = 1; % latency class (0 - 3)
%    buffersize = 2048; % buffersize
    dev = PsychPortAudio('GetDevices'); % get list of all devices
    for k=1:length(dev), if contains(dev(k).DeviceName,'ASIO Fireface USB'), devID = k-1; end, end;
    pamaster = PsychPortAudio('Open', devID, mode+8, reqlatencyclass, fsamp,[MasterChanPlay; MasterChanRec],buffersize,0.005);
    %pahandle = PsychPortAudio(�Open� [, deviceid][, mode][, reqlatencyclass][, freq][, channels][, buffersize][, suggestedLatency][, selectchannels][, specialFlags=0]);
    PsychPortAudio('Start', pamaster,0,0,1);
    
    pahandle = PsychPortAudio('OpenSlave',pamaster,mode,[length(PlayChanList); length(RecChanList)],[PlayChanList;RecChanList]);
    InputSig = input.';
    
    PsychPortAudio('FillBuffer', pahandle, InputSig);
    %[underflow, nextSampleStartIndex, nextSampleETASecs] = PsychPortAudio(�FillBuffer�, pahandle, bufferdata [, streamingrefill=0][, startIndex=Append]);
    TimeDur = size(InputSig,2)*1/fsamp + 0.2;
    PsychPortAudio('GetAudioData', pahandle, TimeDur, 0.1, 0.1);
    %[audiodata absrecposition overflow cstarttime] = PsychPortAudio(�GetAudioData�, pahandle [, amountToAllocateSecs][, minimumAmountToReturnSecs][, maximumAmountToReturnSecs][, singleType=0]);
    PsychPortAudio('Start', pahandle,1,0,0);
    
    WaitSecs(TimeDur); % pause to send data and receive signal
    Status= PsychPortAudio('GetStatus', pahandle);
    [input absrecposition overflow cstarttime] = PsychPortAudio('GetAudioData', pahandle);
    
    
    [startTime endPositionSecs xruns estStopTime] = PsychPortAudio('Stop', pahandle, 1);
    %[startTime endPositionSecs xruns estStopTime] = PsychPortAudio(�Stop�, pahandle [, waitForEndOfPlayback=0] [, blockUntilStopped=1] [, repetitions] [, stopTime]);
    PsychPortAudio('Close');% Close the audio device:
    clear PsychPortAudio; % clear portaudio
    RecChirp = (input).';
   
    if xruns>0||Status.XRuns||overflow
       fprintf("Under/overrun detected");
    end