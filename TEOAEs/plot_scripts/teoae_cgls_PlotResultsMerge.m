% teoae decompose
clear all;
addpath('D:\Experimenty\oaes_v2\Results') % google drive data do diplomky  - ZMENIT CESTU

% pathname = uigetdir('..\Results\pokus\fstest');
%%
pathname = uigetdir('..\Results\s003\TEOAE_CGLS\22_02');
%pathname = 'Results/'
FldInf = dir(pathname);
%%
LSB = findobj('tag','listboxRecFiles'); % handel to listbox where the values of F1 F2 L1 a L2
% will be given (data in experiments)

Lvar=40:5:70;
IndTeoaeStart=1;

for kk=1:length(Lvar)
    WholerecordedSignal=[];
    for j=1:size(FldInf,1)
        Lploc = strfind(FldInf(j).name,['L_' num2str(Lvar(kk))]);
        if (~isempty(Lploc))
            load([pathname filesep FldInf(j).name], '-regexp','^(?!WholerecordedSignal|hh|handles|hObject|eventdata$)\w' ); %|b'^(?!Wholerecorded)...'
            % load recordedSignalFolderInfo(j).name
            WholerecordedSignal=[WholerecordedSignal; recordedSignal(4209:end-400,:)];
        end
    end

    fcutoff =100; % cutoff freq of 2order high pass butterworth filter
    [b,Roae] = butter(4,fcutoff/(fs/2),'high');
    % RSfilted = filter(b,Roae,recordedSignal(:,1));
    % RSfilted = filter(b,Roae,recordedSignal);
    RSfilted = filtfilt(b,Roae,WholerecordedSignal);
    % RSf = RSfilted(3141:end);
    RSf = RSfilted;
    NumOfClicks=floor(length(RSf)/length(CEOAEBlock));
    % NumOfClicks=NumOfPresentedClicks-2;
    % NumOfClicks=299;
    % CEOAEBlock=zeros(1,4800);
    RSf = RSf(1:NumOfClicks*length(CEOAEBlock));
    Rsh = reshape(RSf,length(CEOAEBlock),NumOfClicks);
    
    deleteWholeRows=1;
    if deleteWholeRows==1
        RshE=sum(Rsh.^2); %energy of Rsh signal
        ThRshE=median(RshE)*60; %threshold of Rsh energy signal to remove the high energy trials
        idx = find(RshE > ThRshE);
        Rsh(:,idx) = NaN;
    end
    %
    % %% Artifact rejection procedure according to Yi-Wen Liu
    % % A point-wise artifact rejection method for estimating
    % % transient-evoked otoacoustic emissions and their group delay
    %
    % %  Frame exclusion
    % % Roae = Rsh(550:1700,:); %doresit
    ifcompute=1;
    Roae = Rsh; %doresit
    
    if ifcompute
        Roae = Rsh(IndTeoaeStart:3500,:); %doresit
        [MaxRsh,IndMaxRsh] = max((nanmean(Rsh')));
        tau=(IndMaxRsh-IndTeoaeStart)/fs; %latency between click and TEOAE
        [idxx, idxy] = find(abs(Roae) > 2e-2);
        for ii=1:length(idxx)
            Roae(idxx(ii),idxy(ii))=NaN;
        end
        RoaeMean = nanmean(Roae.');
        
        %  Noise shaping
        noiseMat=(Roae-RoaeMean')';
        Roae=Roae';
        % figure;histogram(noiseMat(801,:));
        numSamples=length(noiseMat(1,:));
        sigma=sqrt(nanmean(mean(noiseMat'.^2)));
        % sigma=sqrt(nanmean(nanmean(noiseMat'.^2)));
        Theta=2*sigma;
        
        [idxx, idxy] = find(abs(noiseMat) > Theta);
        for ii=1:length(idxx)
            noiseMat(idxx(ii),idxy(ii))=NaN;
            Roae(idxx(ii),idxy(ii))=NaN;
        end
    end

    Teoae_time=nanmean(Roae);
    
    % Kolmogorov-Smirnof test
    % hV=zeros(1,numSamples);
    % pV=zeros(1,numSamples);
    % for ii=1:numSamples
    %     [h,p] = kstest(noiseMat(:,ii));
    %     hV(ii)=h;
    %     pV(ii)=p;
    % end
    
    
    % Teoae_time_zeros = [Teoae_time, zeros(1,1e4)];
    Teoae_time_zeros = [Teoae_time.*tukeywin(length(Teoae_time),0.1)', zeros(1,1e4)];
    
    fxI = (0:length(Teoae_time_zeros)-1)*fs/length(Teoae_time_zeros); % frequency axis
    % figure; plot(fxI,abs(fft(Teoae_time_zeros)))
    
    % HoaemicsensX = interp1(fx,Hoaemicsens,fxI,'spline');
    % Teoae=fft(Teoae_time_zeros)/length(Teoae_time_zeros)./HoaemicsensX;
    Teoae=fft(Teoae_time_zeros)/length(Teoae_time_zeros);
    
    % figure(11),subplot(211), hold on, plot(fxI, (20*log10(abs(Teoae)/2e-5)+10*(length(LList)-kk))), xlim([1e3 4e3])
    figure(11),subplot(311), hold on, plot(fxI, (20*log10(abs(Teoae)))), xlim([1e3 4e3])
    SpectXLim=get(gca,'XLim');
    SpectXLimLowInd = find(fxI>SpectXLim(1),1);
    SpectXLimUppInd = find(fxI>SpectXLim(2),1);
    title('Spectra')
    figure(11),subplot(312), hold on, plot(linspace(IndTeoaeStart/fs,length(Teoae_time)/fs,length(Teoae_time)),Teoae_time)
    title('TEOAE amplitude')
    % figure(),subplot(211), hold on, plot(fxI, 20*log10(abs(Teoae)/2e-5)), xlim([1e3 4e3])
%     figure(11),subplot(313), hold on,plot(fxI(SpectXLimLowInd:SpectXLimUppInd), gradient(unwrap(angle(Teoae(SpectXLimLowInd:SpectXLimUppInd).*exp(1*sqrt(-1).*fxI(SpectXLimLowInd:SpectXLimUppInd)*2*pi*tau ))))/2/pi)%, xlim([1e3 4e3]), ylim([-45 0])
    figure(11),subplot(313), hold on,plot(fxI(SpectXLimLowInd:SpectXLimUppInd),-unwrap(angle(Teoae(SpectXLimLowInd:SpectXLimUppInd).*exp(1*sqrt(-1).*fxI(SpectXLimLowInd:SpectXLimUppInd))))/2/pi),xlim(SpectXLim)%, xlim([1e3 4e3]), ylim([-45 0])
    title('Phase')

end
legend(string(Lvar))

hh = findall( 0, 'type', 'figure', 'Name', 'teoaeMain' );  % Find figures
hh.delete;
clear hh handles hObject eventdata

% k = find(WholerecordedSignal(:,2)>0.05925) ;
% figure,plot(diff(k))
