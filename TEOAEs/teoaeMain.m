function varargout = teoaeMain(varargin)
% TEOAEMAIN MATLAB code for teoaeMain.fig
%      TEOAEMAIN, by itself, creates a new TEOAEMAIN or raises the existing
%      singleton*.
%
%      H = TEOAEMAIN returns the handle to a new TEOAEMAIN or the handle to
%      the existing singleton*.
%
%      TEOAEMAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TEOAEMAIN.M with the given input arguments.
%
%      TEOAEMAIN('Property','Value',...) creates a new TEOAEMAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before teoaeMain_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to teoaeMain_OpeningFcn via varargin.
%prese
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help teoaeMain

% Last Modified by GUIDE v2.5 01-Feb-2023 10:45:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1 ;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @teoaeMain_OpeningFcn, ...
    'gui_OutputFcn',  @teoaeMain_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before teoaeMain is made visible.
function teoaeMain_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to teoaeMain (see VARARGIN)

% Choose default command line output for teoaeMain
handles.output = hObject;

% setenv('PATH', [getenv('PATH') ';E:\Experimenty\OAES\Dev\PsychPortAudio3']);
%setenv('PATH', [getenv('PATH') ';PsychPortAudio3/']);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes teoaeMain wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = teoaeMain_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;





function L1e_Callback(hObject, eventdata, handles)
% hObject    handle to L1e (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L1e as text
%        str2double(get(hObject,'String')) returns contents of L1e as a double


% --- Executes during object creation, after setting all properties.
function L1e_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L1e (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function WinClickDure_Callback(hObject, eventdata, handles)
% hObject    handle to WinClickDurtext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of WinClickDurtext as text
%        str2double(get(hObject,'String')) returns contents of WinClickDurtext as a double


% --- Executes during object creation, after setting all properties.
function WinClickDurtext_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WinClickDurtext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function stimdurV_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stimdurV (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function NumOfPresentedClickse_Callback(hObject, eventdata, handles)
% hObject    handle to NumOfPresentedClickse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NumOfPresentedClickse as text
%        str2double(get(hObject,'String')) returns contents of NumOfPresentedClickse as a double


% --- Executes during object creation, after setting all properties.
function NumOfPresentedClickse_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NumOfPresentedClickse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ResDirbutt.
function ResDirbutt_Callback(hObject, eventdata, handles)
% hObject    handle to ResDirbutt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

startpath = 'Results';
ResDirExist = exist (['Results']);
% if ResDirExist~=7   %if 7 then ..\Results exists
%     mkdir Results;
% end
dirname = uigetdir(startpath, 'choose folder where to save results');

set(handles.ResDire,'string',dirname);

function ResDire_Callback(hObject, eventdata, handles)
% hObject    handle to ResDire (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ResDire as text
%        str2double(get(hObject,'String')) returns contents of ResDire as a double


% --- Executes during object creation, after setting all properties.
function ResDire_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ResDire (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function subnamee_Callback(hObject, eventdata, handles)
% hObject    handle to subnamee (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of subnamee as text
%        str2double(get(hObject,'String')) returns contents of subnamee as a double


% --- Executes during object creation, after setting all properties.
function subnamee_CreateFcn(hObject, eventdata, handles)
% hObject    handle to subnamee (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function fsampe_Callback(hObject, eventdata, handles)
% hObject    handle to fsampe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fsampe as text
%        str2double(get(hObject,'String')) returns contents of fsampe as a double


% --- Executes during object creation, after setting all properties.
function fsampe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fsampe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function micgaine_Callback(hObject, eventdata, handles)
% hObject    handle to micgaine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of micgaine as text
%        str2double(get(hObject,'String')) returns contents of micgaine as a double


% --- Executes during object creation, after setting all properties.
function micgaine_CreateFcn(hObject, eventdata, handles)
% hObject    handle to micgaine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ChirpEare.
function ChirpEare_Callback(hObject, eventdata, handles)
% hObject    handle to ChirpEare (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% present a chirp signal to ear to get frequency response

Hgain = findobj('tag','micgaine'); % get dB gain of OAE probe preamp
GainMic = str2num(get(Hgain,'string')); % gain of the OAE probe mic (0, 20, 40 dB)
FPL = get(handles.radioInEarFPL,'value'); % FPL kalibrace
SPL = get(handles.radioInEarSPL,'value'); % SPL kalibrace
EarSim = get(handles.tagEarSim,'value');  % ear sim 1 - ano, 0 - ne

O_SPL = get(handles.radioOAEspl,'value'); % SPL for OAE
O_EPL = get(handles.radioOAEepl,'value'); % EPL for OAE

if EarSim && ~isfield(handles,'LLT')  % if LLT was not loaded
    disp('you must load LLT response for the used ear plug');
    return;
end

if isfield(handles,'LLT')  % if LLT was not loaded
    LLT = handles.LLT;  % LLT response
else
    LLT = [];
end

DT = datetime('now'); % get current time and date
Folder = get(handles.ResDire,'string'); % get directory where to save recorded signals
SubjName = get(handles.subnamee,'string'); % get name of the subject
Year = DT.Year-2000;
YearS = num2str(Year);
Month = DT.Month;


if Month<10, MonthS = ['0' num2str(Month)]; else, MonthS = [num2str(Month)];end
Day = DT.Day;
if Day<10,DayS = ['0' num2str(Day)]; else, DayS = [num2str(Day)]; end
Hour = DT.Hour;
if Hour<10,  HourS = ['0' num2str(Hour)];else, HourS = [num2str(Hour)]; end
Minute = DT.Minute;
if Minute<10, MinuteS = ['0' num2str(Minute)];else, MinuteS = [num2str(Minute)];end
Second = round(DT.Second);
if Second<10,SecondS = ['0' num2str(Second)];else, SecondS = [num2str(Second)];end

NumChirps = 128; % number of chirps
Amp = 0.05;
%save([Folder '\rec_' SubjName '_' YearS MonthS DayS '_' HourS MinuteS SecondS ...

% call function for calibration with parameters:
% GainMic, EarSim, O_SPL, O_EPL, FPL, SPL,
% NumChirps,fsamp,Nsamp,Folder,SubjName,Years,Months,Days,Hours,Minutes,Seconds

fsamp = str2double(get(handles.fsampe,'string')).*1e3;
Nsamp = 2048; % number of samples in the chirp
% [f1tone, f2tone] = creat

addpath('fce_functions');
playChirptoEar(Amp,GainMic,NumChirps,fsamp,Nsamp,FPL,O_EPL,EarSim,Folder,SubjName,YearS,MonthS,DayS,HourS,MinuteS,SecondS,LLT);

% --- Executes on button press in TEOAEe.
function TEOAEe_Callback(hObject, eventdata, handles)
% hObject    handle to TEOAEe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

addpath('fce_functions/'); addpath('fce_stimuli/'); addpath('fce_psychportaudio/')
addpath('fce_waveletdecomposition/')
addpath('PsychPortAudio3/')

ClickCalibType = get(handles.ClickCalibse,'value');
ClickCalibTypeStr=get(handles.ClickCalibse,'string');
ClickCalibTypeStr=ClickCalibTypeStr{ClickCalibType};
if ClickCalibType==2
    if str2num(get(handles.L1e,'string'))>79
        msgbox(['sampling frequency was set to 96 kHz due to using CGLS'...
            newline newline 'too high stimulus intensity, the probe does not work linearly at >75 dB SPL'] ,'');
    else
        set(handles.fsampe,'String','96');
        fsbox=msgbox('sampling frequency was set to 96 kHz due to using CGLS','Sampling frequency');
        
     end
end

L1 = str2num(get(handles.L1e,'string'));
NumOfPresentedClicks = str2num(get(handles.NumOfPresentedClickse,'string'));
WinClickDur = str2num(get(handles.WinClickDure,'string'))/1e3;
fs = 1e3*str2num(get(handles.fsampe,'string')); % sampling freq

GainMic = str2num(get(handles.micgaine,'string')); % gain of the OAE probe mic (0, 20, 40 dB)
GainMicA = 10^(GainMic/20);

WhichEar = get(handles.RLear,'value');
if WhichEar==1
    WhichEar='_Right';
elseif WhichEar==2
    WhichEar='_Left';
end


SaveResultsAndBreak=1;
DT = datetime('now'); % get current time and date
Folder = get(handles.ResDire,'string'); % get directory where to save recorded signals
SubjName = get(handles.subnamee,'string'); % get name of the subject
Year = DT.Year-2000;
YearS = num2str(Year);
Month = DT.Month;


if Month<10, MonthS = ['0' num2str(Month)]; else, MonthS = [num2str(Month)];end
Day = DT.Day;
if Day<10,DayS = ['0' num2str(Day)]; else, DayS = [num2str(Day)]; end
Hour = DT.Hour;
if Hour<10,  HourS = ['0' num2str(Hour)];else, HourS = [num2str(Hour)]; end
Minute = DT.Minute;
if Minute<10, MinuteS = ['0' num2str(Minute)];else, MinuteS = [num2str(Minute)];end
Second = round(DT.Second);
if Second<10,SecondS = ['0' num2str(Second)];else, SecondS = [num2str(Second)];end




load Calibration/inEarCalibration.mat  Hres1 Hres2 fx % load in ear calibration to save it to results
% load Calibration\CalibMicData.mat Hoaemicsens;
load Calibration\CalibMicDataSDist16mmYellowFoam13mm.mat Hoaemicsens fx



[CEOAEclickVar ] = genCEOAEclick(L1,fs,WinClickDur,ClickCalibType);
%       [CEOAEclickRef] = genCEOAEclick(81,fs);
%       CEOAEBlock=[CEOAEclickVar CEOAEclickRef];
CEOAEBlock=[ CEOAEclickVar ];
% CEOAEXBlocks = repmat(CEOAEBlock,1,NumOfPresentedClicks);
numMeasurements=ceil(NumOfPresentedClicks/(10/(length(CEOAEBlock)/fs)));
% numMeasurements = 1;
% CEOAEXBlocks = repmat(CEOAEBlock,1,NumOfPresentedClicks);
CEOAEXBlocks = [repmat(CEOAEBlock,1,NumOfPresentedClicks/numMeasurements), zeros(1,3140-276)];

WholerecordedSignal=[];
setappdata(handles.figure1,'stopPlot',0);
tic
for k = 1:numMeasurements
    set(handles.text15,'String',[num2str(k), '/' , num2str(numMeasurements)])
    drawnow();
    
    [recordedSignal] = presentTEOAEs(CEOAEXBlocks,fs); %,handles.gen
   save([Folder filesep 'TEOAE_' SubjName '_' YearS MonthS DayS '_' HourS MinuteS SecondS ...
    '_L_' num2str(L1) '_WinClickDur_' num2str(round(WinClickDur*1e3)) ...
    '_NumOfPresentedClicks_' num2str(NumOfPresentedClicks) WhichEar ...
    '_Calib_' ClickCalibTypeStr ,  '_', num2str(k),'.mat'], '-regexp','^(?!WholerecordedSignal|hh|handles|hObject|eventdata$)\w' );
    if getappdata(handles.figure1,'stopPlot')==1
        break;
    end
end
toc
% compare with previous signal
%     figure;plot(CEOAEXBlocks)
%     hold on
%     plot(recordedSignal(:,1))
%     hold on
%     plot(CEOAEclickVar)
if SaveResultsAndBreak==0
WholerecordedSignal = recordedSignal(3141:end,1);
fcutoff =100; % cutoff freq of 2order high pass butterworth filter
[b,Roae] = butter(4,fcutoff/(fs/2),'high');
% RSfilted = filter(b,Roae,recordedSignal(:,1));
% RSfilted = filter(b,Roae,recordedSignal);
RSfilted = filtfilt(b,Roae,WholerecordedSignal);
% RSf = RSfilted(3141:end);
RSf = RSfilted;
NumOfClicks=floor(length(RSf)/length(CEOAEBlock));
RSf = RSf(1:NumOfClicks*length(CEOAEBlock));
Rsh = reshape(RSf,length(CEOAEBlock),NumOfClicks);
% figure; imagesc(Rsh)
% figure; plaot(Rsh)

deleteWholeRows=0;
if deleteWholeRows==1
    RshE=sum(Rsh.^2); %energy of Rsh signal
    ThRshE=median(RshE)*1.03; %threshold of Rsh energy signal to remove the high energy trials
    idx = find(RshE > ThRshE);
    Rsh(:,idx) = NaN;
end

%% Artifact rejection procedure according to Yi-Wen Liu
% A point-wise artifact rejection method for estimating
% transient-evoked otoacoustic emissions and their group delay

%  Frame exclusion
% Roae = Rsh(550:1700,:); %doresit
%IndTeoaeStart=550;
IndTeoaeStart = 414; % click center position

Roae = Rsh(IndTeoaeStart:1700,:); %doresit
[MaxRsh,IndMaxRsh] = max((nanmean(Rsh')));
tau=(IndMaxRsh-IndTeoaeStart)/fs; %latency between click and TEOAE
% tukeywin(XXXXX,0.05)
[idxx, idxy] = find(abs(Roae) > 2e-3);
for ii=1:length(idxx)
    Roae(idxx(ii),idxy(ii))=NaN;
end
RoaeMean = nanmean(Roae.');

%  Noise shaping
noiseMat=(Roae-RoaeMean')';
Roae=Roae';
% figure;histogram(noiseMat(801,:));
numSamples=length(noiseMat(1,:));
sigma=sqrt(nanmean(mean(noiseMat'.^2)));
% sigma=sqrt(nanmean(nanmean(noiseMat'.^2)));
Theta=2*sigma;

[idxx, idxy] = find(abs(noiseMat) > Theta);
for ii=1:length(idxx)
    noiseMat(idxx(ii),idxy(ii))=NaN;
    Roae(idxx(ii),idxy(ii))=NaN;
end

Teoae_time=nanmean(Roae);
noiseMat_time = nanmean(noiseMat);
% Kolmogorov-Smirnof test
% hV=zeros(1,numSamples);
% pV=zeros(1,numSamples);
% for ii=1:numSamples
%     [h,p] = kstest(noiseMat(:,ii));
%     hV(ii)=h;
%     pV(ii)=p;
% end

Teoae_time(isnan(Teoae_time)) = 0;
noiseMat_time(isnan(noiseMat_time)) = 0;
Teoae_time_zeros = [Teoae_time(200:end).'.*tukeywin(length(Teoae_time(200:end)),0.01); zeros(1e4,1)];
noiseMat_zeros = [noiseMat_time(200:end).'.*tukeywin(length(noiseMat_time(200:end)),0.01); zeros(1e4,1)];
% Teoae_time_zeros = [Teoae_time, zeros(1,1e4)];

fxI = (0:length(Teoae_time_zeros)-1)*fs/length(Teoae_time_zeros); % frequency axis
%figure; plot(fxI,20*log10(abs(fft(Teoae_time_zeros))))

HoaemicsensX = interp1(fx,Hoaemicsens,fxI,'spline');
Teoae=fft(Teoae_time_zeros)/length(Teoae_time_zeros)./HoaemicsensX;
Teoaenoise = fft(noiseMat_zeros)/length(noiseMat_zeros)./HoaemicsensX;
clear hObject eventdata
% save (['Results/s003/TEOAE_16_6_2022/L1_',num2str(L1),'.mat'])
% save([Folder filesep SubjName filesep 'TEOAE' filesep 'TEOAE_' SubjName '_' YearS MonthS DayS '_' HourS MinuteS SecondS ...
%     '_L_' num2str(L1) '_WinClickDur_' num2str(round(WinClickDur*1e3)) ...
%     '_NumOfPresentedClicks_' num2str(NumOfPresentedClicks) '_' WhichEar ...
%     '.mat']);
figure(99)
hold on
plot(fxI, 20*log10(abs(Teoae)/(sqrt(2)*2e-5)));
plot(fxI, 20*log10(abs(Teoaenoise)/(sqrt(2)*2e-5)));
hold off


xlim([0.5e3 4e3])
end
ifplot=0;

set(handles.text15,'String','0');
drawnow();

% info to the person, measuement finished
msgbox('measurement is finished','');


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over TEOAEe.
function TEOAEe_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to TEOAEe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function text15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on selection change in RLear.
function RLear_Callback(hObject, eventdata, handles)
% hObject    handle to RLear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns RLear contents as cell array
%        contents{get(hObject,'Value')} returns selected item from RLear


% --- Executes during object creation, after setting all properties.
function RLear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RLear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function treshold_Callback(hObject, eventdata, handles)
% hObject    handle to treshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of treshold as text
%        str2double(get(hObject,'String')) returns contents of treshold as a double


% --- Executes during object creation, after setting all properties.
function treshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to treshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK_treshold.
function OK_treshold_Callback(hObject, eventdata, handles)
% hObject    handle to OK_treshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in RLear.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to RLear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns RLear contents as cell array
%        contents{get(hObject,'Value')} returns selected item from RLear


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RLear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% function RLear_Callback(hObject, eventdata, handles)
% hObject    handle to RLear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns RLear contents as cell array
%        contents{get(hObject,'Value')} returns selected item from RLear


% --- Executes during object creation, after setting all properties.
% function RLear_CreateFcn(hObject, eventdata, handles)
% % hObject    handle to RLear (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    empty - handles not created until after all CreateFcns called
%
% % Hint: popupmenu controls usually have a white background on Windows.
% %       See ISPC and COMPUTER.
% if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
%     set(hObject,'BackgroundColor','white');
% end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in tagStopM.
function tagStopM_Callback(hObject, eventdata, handles)
% hObject    handle to tagStopM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

setappdata(handles.figure1,'stopPlot',1);
% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in tagSOAE.
function tagSOAE_Callback(hObject, eventdata, handles)
% hObject    handle to tagSOAE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

addpath('fce_functions/');  addpath('fce_psychportaudio/'); addpath('PsychPortAudio3/')

disp('SOAE measurement has started')
fsamp = str2double(get(handles.fsampe,'string')).*1e3;
% [f1tone, f2tone] = createDPOAEstimuli(1000,1000,10,10,fsamp,30,0.05);
%
% f1tone = f1tone*0; % a stupid way how not to produce a sound (produce a silent sound)


DSP = 2;

if DSP==0 % daq
    % create DirectSound session
    session = daq.createSession('directsound');
    
    % get devices and set the input and output device
    devices = daq.getDevices;
    
    % input_device = devices(3);
    input_device = devices(10); % Mic input - settings for lab n. 541
    
    output_device = devices(5);
    %output_device = devices(12); % Phones - settings for lab n. 541
    
    nchan = 2;
    
    % add input and output channels
    chi=addAudioInputChannel(session,input_device.ID,1:nchan);
    % chi=addAudioInputChannel(session,input_device.ID,1);
    cho=addAudioOutputChannel(session,output_device.ID,1:nchan);
    
    session.Rate = fsamp;
    %session.IsContinuous = true;
    outputData = zeros(length(f1tone),nchan);
    
    %Prepare data for output
    queueOutputData(session, [f1tone f1tone]);
    
    %Preparing session - reduce latency
    prepare(session);
    
    %starting Data Aquisition and recording data
    %[data,timeStamps,triggerTime] = startForeground(session);
    [nahravka,timeStamps,triggerTime] = startForeground(session);
    
    stop(session);
    
    % session.IsContinuous = false;
    % release DirectSound session
    release(session);
    
elseif DSP==1
    
    
    H = dsp.AudioPlayer
    
    devID = 'default';                    %device ID The default is Default, which
    %is the computer's standard output device.
    %You can use tab completion to query valid
    %DeviceName assignments for your computer by
    %typing H.DeviceName = ' and then pressing the tab key.
    bitDepth = '24-bit integer';  %output bitdepth
    %outputCh = [1,2];             %vector containing output channels' numbers
    bufferSize = 2048;
    %
    % data.fs = 48e3;
    Fs= fsamp;
    playDeviceID = 'default';
    recDeviceID = 'default';
    RecChanList = 1; %str2double(get(handles.recChan,'string'));
    PlayChanList = [3,4];
    
    nahravka = DSPplayANDrec([0*f1tone 0*f2tone (f1tone + f2tone)/10],playDeviceID,PlayChanList,recDeviceID,RecChanList,bufferSize,Fs);
    
elseif DSP == 2 % psychportaudio
    %     addpath('PsychPortAudio3/')
    %     clear PsychPortAudio; % clear portaudio (
    %     PsychPortAudio('Verbosity', 5); %
    %oldlevel = PsychPortAudio(‘Verbosity’ [,level]);
    %Set level of verbosity for error/warning/status messages. ‘level’ optional, new
    %level of verbosity. ‘oldlevel’ is the old level of verbosity. The following
    %levels are supported: 0 = Shut up. 1 = Print errors, 2 = Print also warnings, 3
    %= Print also some info, 4 = Print more useful info (default), >5 = Be very
    %verbose (mostly for debugging the driver itself).
    RecChanList = [1] ; %str2double(get(handles.recChan,'string'));
    
    mode = 2; % duplex mode (play and record)
    reqlatencyclass = 1; % latency class (0 - 3)
    buffersize = 2048; % buffersize
    %     pahandle = PsychPortAudio('Open', 42, mode, reqlatencyclass, fsamp,[length(RecChanList)],buffersize,0.005,[RecChanList]);
    %pahandle = PsychPortAudio(‘Open’ [, deviceid][, mode][, reqlatencyclass][, freq][, channels][, buffersize][, suggestedLatency][, selectchannels][, specialFlags=0]);
    
    TimeDur = 60;
    %     PsychPortAudio('GetAudioData', pahandle, TimeDur, 0.1, 0.1);
    %     %[audiodata absrecposition overflow cstarttime] = PsychPortAudio(‘GetAudioData’, pahandle [, amountToAllocateSecs][, minimumAmountToReturnSecs][, maximumAmountToReturnSecs][, singleType=0]);
    %     PsychPortAudio('Start', pahandle,1,0,0);
    %     pause(TimeDur); % pause to send data and receive signal
    %     [input absrecposition overflow cstarttime] = PsychPortAudio('GetAudioData', pahandle, []);
    %
    %     Status= PsychPortAudio('GetStatus', pahandle);
    %     [startTime endPositionSecs xruns estStopTime] = PsychPortAudio('Stop', pahandle, 1);
    %     %[startTime endPositionSecs xruns estStopTime] = PsychPortAudio(‘Stop’, pahandle [, waitForEndOfPlayback=0] [, blockUntilStopped=1] [, repetitions] [, stopTime]);
    %     PsychPortAudio('Close', pahandle);% Close the audio device:
    %     clear PsychPortAudio; % clear portaudio
    %     RecOAE = (input([1],:))';
    RecOAE = PPArec(buffersize,RecChanList,fsamp,TimeDur);
    
    %     if Status.XRuns>0
    %         fprintf('Status: xrun detected\n');
    %     end
    %
    %     if xruns>0
    %         fprintf('xrun detected\n');
    %     end
end


disp('SOAE measurement has finished')


GainMic = str2double(get(handles.micgaine,'string')); % gain of the OAE probe mic (0, 20, 40 dB)
GainMicA = 10^(GainMic/20);
%
% [CalSpect,fxI] = getRecSpectrum(nahravka,fsamp,GainMicA); % get spectrum
%
%
%

fcutoff =300; % cutoff freq of 2order high pass butterworth filter
[b,a] = butter(4,fcutoff/(fsamp/2),'high');
RSfilted = filter(b,a,RecOAE);

[CalSpect,fxI] = getRecSOAESpectrum(RSfilted,fsamp,GainMicA);

%fx = linspace(0,fsamp,length(RecOAE));
figure;
plot(fxI,20*log10(abs(CalSpect)/(sqrt(2)*2e-5))); % plot spectrum in dB SPL
xlabel('Frequency (Hz)');
ylabel('Amplitude (dB SPL)');

uisave({'RecOAE','fsamp', 'GainMicA'},'Results/')

rmpath('fce_functions/');  rmpath('fce_psychportaudio/'); rmpath('PsychPortAudio3/')


% --- Executes on button press in tagLoadLLT.
function tagLoadLLT_Callback(hObject, eventdata, handles)
% hObject    handle to tagLoadLLT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uigetfile('Calibration/');
load([pathname filename]);

LLT.fx = fxI;
LLT.Hres1 = Hres1;
LLT.Hres2 = Hres2;

handles.LLT = LLT;
guidata(hObject, handles);


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tagLoadLLT.
function tagLoadLLT_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tagLoadLLT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on button press in tagEarSim.
function tagEarSim_Callback(hObject, eventdata, handles)
% hObject    handle to tagEarSim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tagEarSim


% --- Executes on button press in tagAGram.
function tagAGram_Callback(hObject, eventdata, handles)
% hObject    handle to tagAGram (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

addpath('../BALaudiogramOAE')
% cd ../BALaudiogramOAE
audiogram;

% cd ../oaes_v2
% rmpath('../BALaudiogramOAE')



% --- Executes during object creation, after setting all properties.
function WinClickDure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WinClickDure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ClickCalibse.
function ClickCalibse_Callback(hObject, eventdata, handles)
% hObject    handle to ClickCalibse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ClickCalibse contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ClickCalibse


% --- Executes during object creation, after setting all properties.
function ClickCalibse_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ClickCalibse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
